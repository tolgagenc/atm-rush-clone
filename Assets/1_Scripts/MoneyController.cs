using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyController : MonoBehaviour
{
    public GameObject Gold;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Door")
        {
            GameObject replaceGld = Instantiate(Gold, transform.position, Quaternion.identity, transform);
            replaceGld.transform.localPosition = transform.localPosition;
            replaceGld.transform.localScale = transform.localScale;
            Destroy(transform.gameObject);
        }
        if (other.gameObject.tag == "ATM")
        {
            Preferences.count++;
            Destroy(transform.gameObject);
        }
    }
}
