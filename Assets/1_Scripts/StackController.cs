using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackController : MonoBehaviour
{
    private Vector3 nextPos;
    private Vector3 firstPos;
    private SphereCollider sphereCollider;

    // Start is called before the first frame update
    void Start()
    {
        sphereCollider = transform.GetComponent<SphereCollider>();

        firstPos = transform.position;

        nextPos = new Vector3(0, -0.25f, transform.position.z + 2.6f);
    }

    // Update is called once per frame
    void Update()
    {
        if( transform.childCount > 0 )
        {
            transform.position = new Vector3(firstPos.x, firstPos.y, transform.GetChild(transform.childCount-1).transform.position.z + 1.7f);
        }
        else if( transform.childCount == 0)
        {
            transform.position = firstPos;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if( other.gameObject.tag == "Money")
        {
            other.gameObject.transform.SetParent(gameObject.transform);

            other.gameObject.transform.localPosition = nextPos;

            nextPos = new Vector3(0, -0.25f, nextPos.z + 1.7f);

            sphereCollider.center = nextPos;
        }

        if (other.gameObject.tag == "Gold")
        {
            other.gameObject.transform.SetParent(gameObject.transform);

            other.gameObject.transform.localPosition = nextPos;

            nextPos = new Vector3(0, -0.25f, nextPos.z + 1.7f);

            sphereCollider.center = nextPos;
        }

        if (other.gameObject.tag == "Diamond")
        {
            other.gameObject.transform.SetParent(gameObject.transform);

            other.gameObject.transform.localPosition = nextPos;

            nextPos = new Vector3(0, -0.25f, nextPos.z + 1.7f);

            sphereCollider.center = nextPos;
        }
    }
}
