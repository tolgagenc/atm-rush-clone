using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float minRoadX = -3.2f;
    float maxRoadX = 3.2f;

    float minRoadZ;
    float maxRoadZ;

    float screenDis;
    float roadDisX;
    float roadDisZ;
    float disDifX;
    float disDifZ;

    float minDis;
    float maxDis;
    float? firstMousePosX;

    public float playerSpeed = 5f;

    private Vector3 playerVelocity;

    private Material material;

    // Start is called before the first frame update
    void Start()
    {
        screenDis = (Screen.width * 7.5f / 10f) - (Screen.width * 2.5f / 10f);

        roadDisX = Mathf.Abs(maxRoadX - minRoadX);
        roadDisZ = Mathf.Abs(maxRoadZ - minRoadZ);

        disDifX = screenDis / roadDisX;
        disDifZ = screenDis / roadDisZ;

        playerVelocity = transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        MouseControlX();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * playerSpeed);
    }

    void MouseControlX()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstMousePosX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButton(0))
        {
            if (firstMousePosX > Input.mousePosition.x)    // Sol
            {
                minDis = transform.position.x - (((float)firstMousePosX - Input.mousePosition.x) / disDifX);
                if (minDis >= minRoadX)
                {
                    transform.position = new Vector3(minDis, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    transform.position = new Vector3(minRoadX, transform.position.y, transform.position.z);
                }
            }
            else if (firstMousePosX < Input.mousePosition.x)   // Sa�
            {
                maxDis = transform.position.x + ((Input.mousePosition.x - (float)firstMousePosX) / disDifX);
                if (maxDis <= maxRoadX)
                {
                    transform.position = new Vector3(maxDis, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    transform.position = new Vector3(maxRoadX, transform.position.y, transform.position.z);
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            firstMousePosX = null;
        }
    }
}
