using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldController : MonoBehaviour
{
    public GameObject Diamond;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if ( other.gameObject.tag == "Door" )
        {
            GameObject replaceDia = Instantiate(Diamond, transform.position, Quaternion.identity, transform);
            replaceDia.transform.localPosition = transform.localPosition;
            replaceDia.transform.localScale = transform.localScale;
            Destroy(transform.gameObject);
        }
        if (other.gameObject.tag == "ATM")
        {
            Preferences.count += 5;
            Destroy(transform.gameObject);
        }
    }
}
