using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;

    public Transform target;

    private Vector3 offset;

    private void Start()
    {
        offset = transform.position - target.position;
    }

    private void LateUpdate()
    {
        Vector3 pos = Vector3.Lerp(transform.position, target.position + offset, speed);
        transform.position = new Vector3(transform.position.x, pos.y, pos.z);
    }
}
